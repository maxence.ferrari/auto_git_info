import git
import json
import os
import datetime
import argparse
import warnings
import __main__


def log_info(args, *, path=None, **kwargs):
    """
    Create a json file with script, commit, parameters, and time keys.
    The commit is the commit sha and script is the main script executed

    args: An argparse object, None or any serializable objet
    path: Path to the json file. default to 'run_log/current_time.json'
    **kwargs: Add serializable objet to the json with its kwargs as a key

    """
    time = datetime.datetime.now().isoformat()
    if isinstance(args, argparse.Namespace):
        args = args.__dict__
    elif isinstance(args, argparse.ArgumentParser):
        args = args.parse_args().__dict__
    if path:
        if '/' in path:
            os.makedirs(path.rsplit('/', 1)[0], exist_ok=True)
    else:
        if not os.path.isdir('run_log'):
            os.makedirs('run_log')
        path = 'run_log/' + time + '.json'
    with open(path, 'w') as logfile:
        repo = git.Repo(os.path.dirname(os.path.abspath(__main__.__file__)), search_parent_directories=True)
        sha = repo.head.object.hexsha
        branch = repo.head.ref.name
        changes = repo.index.diff(None)
        if len(changes):
            warnings.simplefilter('always', DeprecationWarning)
            warnings.warn(f'{[c.a_path for c in changes]} have differences with current commit',
                          DeprecationWarning, stacklevel=2)
            warnings.simplefilter('default', DeprecationWarning)
        out = {'script': __main__.__file__,
               'commit': sha,
               'branch': branch,
               'parameters': args,
               'time': time}
        out.update({k: v for k, v in kwargs.items() if k not in out})
        json.dump(out, logfile)


if __name__ == '__main__':
    import sys
    import subprocess
    import getopt  # https://github.com/python/cpython/blob/main/Lib/pdb.py#L1950

    opts, args = getopt.getopt(sys.argv[1:], 'h:', ['help', ])

    _usage = """\
    usage: auto_git_info.py [log_path=path.json] pyfile [arg] ...
    automatically log the info of pyfile and its argument in a json file
    
    """

    if not args:
        print('_usage')
        sys.exit(2)

    if any(opt in ['-h', '--help'] for opt, optarg in opts):
        print('_usage')
        sys.exit()

    if '=' in args[0]:
        cmd, path = args[0].split('=')
        if cmd not in 'log_path':
            print('Error: {args[0]} contains an = but is not log_path option')
            sys.exit(2)
        args = args[1:]
    elif args[0] in 'log_path':
        path = args[1]
        args = args[2:]
    else:
        path = None
    sys.argv[:] = args  # Hide "pdb.py" and pdb options from argument list
    __main__.__file__ = os.path.abspath(args[0])
    log_info(args[1:], path=path, parsed_arguments=False)
    subprocess.Popen(['python'] + args).wait(timeout=None)
