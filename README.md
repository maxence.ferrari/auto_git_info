Auto Git info
==============

`auto_git_info.py` is a python module made to save a run parameters and git versions.
It will create a json file with those information.

Installation
------------
To install `auto_git_info.py`
```
pip install git+https://gitlab.lis-lab.fr/maxence.ferrari/auto_git_info.git
```
To uninstall, just type `pip uninstall auto_git_info`

Basic usage
-----

##### Python module

```python
from auto_git_info import log_info
import argparse

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("input", type=str, help="Input file")
    parser.add_argument("--output", type=str, default=None, help="Output file")
    
    args = parser.parse_args()
    log_info(args)
```
This will create a json file in `run_log/` by default, with the current time as a name.
The json file created will be:
```json
{"script": "/home/xxx/Documents/Python/auto_git_info/test_auto_git.py", 
 "commit": "7ce86099e62e7ffdfe77fbbcafbe48b862c269b9", 
 "branch": "master",
 "parameters": {"input": "rnd_input",
                "output": null},
 "time": "2023-06-23T18:04:30.894271"}
```

##### Command line

Just launch the auto_git_info module followed by your python script and its arguments
as usual.

```
python -m auto_git_info your_script.py args1 ... -optn argsn
```

or if you want to change the default path:

```
python -m auto_git_info log_path=path.json your_script.py args1 ... -optn argsn
```
*Note that using this method will add `parsed_arguments=False` to the json*

Advance usage
-------------
#### args
Instead of passing the `Namespace` object from the `ArgumentParser` parser, you can also directly pass the parser itself.
Alternatively, you can pass a dictionary or any serializable object.
You can also use `None` if you don't have any arguments.

Additional kwargs can be passed. The kwargs names will be the keys in the json, and their values will be the associated values.


#### path
If you don't want the default path you can use the `path` named argument to specify the path to save the info.
Any intermediate directories will be created.
