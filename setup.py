from setuptools import setup, find_packages

setup(name='auto_git_info',
      version='1.1.1',
      description='Automatically log your command line arguments and commit sha',
      author='Maxence Ferrari',
      py_modules=['auto_git_info'],
      package_dir={'': 'src'},
      install_requires=['gitpython>=3.1'],
     )
